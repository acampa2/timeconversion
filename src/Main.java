import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Main {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
        /*
         * Write your code here.
         */
        String militaryTime="";
        String[] parts = s.split(":");//hh:mm:ssAM
        String seconds = parts[2].substring(0,2);
        String AMPM = parts[2].substring(2,4);

        if(AMPM.equals("AM") && parts[0].equals("12")){
            militaryTime+="00:";
        }
        else if (AMPM.equals("AM")){
            militaryTime+=parts[0] +":";
        }
        else if(AMPM.equals("PM") && parts[0].equals("12")){
            militaryTime+=parts[0]+":";
        }
        else
            militaryTime+=12+Integer.valueOf(parts[0])+":";

        militaryTime+=parts[1]+":"+seconds;

        return militaryTime;
    }


    public static void main(String[] args) throws IOException {
        //String s = "07:05:45PM";
        String s = "12:00:00PM";

        String result = timeConversion(s);
        System.out.println(result);


    }
}
